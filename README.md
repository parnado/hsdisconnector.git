# HSDisconnector

Windows下炉石传说游戏拔线核心代码

## 编译

依赖 GNAT (GNU Ada) 和 [WinDivert](https://reqrypt.org/windivert.html)

MSYS2环境下编译过程

> $ git clone [https://gitee.com/parnado/hsdisconnector.git](https://gitee.com/parnado/hsdisconnector.git)

> $ wget https://reqrypt.org/download/WinDivert-1.4.3-A.zip

> $ unzip WinDivert-1.4.3-A.zip

> $ cd hsdisconnector

> $ gnatmake hsdisconnector -largs -L../WinDivert-1.4.3-A/x86_64 -lwindivert

## 许可证

使用 [LGPLv3 许可证](LICENSE)

