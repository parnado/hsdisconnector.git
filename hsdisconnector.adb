with System.Win32;          use System.Win32;
with WinDivert;             use WinDivert;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Text_IO;
with Ada.Streams.Stream_IO;
with Ada.Strings.Fixed;
with Interfaces;
with Interfaces.C;

procedure HSDisconnector is
  type IPAddr is record
    Part1, Part2, Part3 : Interfaces.Unsigned_8;
  end record;
  
  use Ada.Streams.Stream_IO;
  use Ada.Strings.Fixed;

  F : File_Type;
  IP : IPAddr;
  IPStr, Filter : Unbounded_String := Null_Unbounded_String;
  hnd : HANDLE;
begin
  Open (F, In_File, Name => "filter");
  while not End_Of_File (F) loop
    IPAddr'Read (Stream (F), IP);
    IPStr := To_Unbounded_String (Trim (IP.Part1'Img, Ada.Strings.Left) & "." &
      Trim (IP.Part2'Img, Ada.Strings.Left) & "." & Trim (IP.Part3'Img, Ada.Strings.Left) & ".");
    if Filter /= Null_Unbounded_String then
      Filter := Filter & " or ";
    end if;
    Filter := Filter & "(ip.DstAddr>" & IPStr & "0 and ip.DstAddr<" & IPStr & "255)";
  end loop;
  Close (F);

  hnd := WinDivertOpen (Interfaces.C.To_C (To_String (Filter)), WINDIVERT_LAYER_NETWORK, 0, 0);
  if hnd = INVALID_HANDLE_VALUE then
    Ada.Text_IO.Put_Line ("error: failed to open the WinDivert device" & GetLastError'Img);
    return;
  end if;
  loop
    null;
  end loop;
end HSDisconnector;