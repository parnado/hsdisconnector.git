with Interfaces;   use Interfaces;
with Interfaces.C; use Interfaces.C;
with System.Win32; use System.Win32;

package WinDivert is

  WINDIVERT_LAYER_NETWORK         : constant Unsigned_32 := 0;
  WINDIVERT_LAYER_NETWORK_FORWARD : constant Unsigned_32 := 1;
  WINDIVERT_LAYER_FLOW            : constant Unsigned_32 := 2;
  WINDIVERT_LAYER_SOCKET          : constant Unsigned_32 := 3;
  WINDIVERT_LAYER_REFLECT         : constant Unsigned_32 := 4;
  
  function WinDivertOpen (filter: in char_array; layer:Unsigned_32; priority:Integer_16; flags:Unsigned_64) return HANDLE
    with Import, Convention => C, External_Name => "WinDivertOpen";

  function WinDivertClose (hnd : HANDLE) return BOOL
    with Import, Convention => C, External_Name => "WinDivertClose";

end WinDivert;